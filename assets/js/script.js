/* Author:http://www.rainatspace.com

*/

jQuery(document).ready(function(){

	//RESPONSIVE NAV
	jQuery('ul#menu').slicknav({
		label: '',
		duration: 1000
	});

	//SELECT
	jQuery('.selectpicker').selectpicker({
		showIcon: false
	});

	//IMG FILL
	jQuery(".imgFill").imgLiquid({fill:true});
	jQuery(".imgNoFill").imgLiquid({fill:false});

	//FILEUPLOAD
	jQuery(":file").filestyle();

	//PORTOFOLIO SLIDER
	jQuery('.bxslider').bxSlider({
  		pagerCustom: '#bx-pager',
  		adaptiveHeight: true
	});
});



